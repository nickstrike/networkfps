﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float range = 2f;
    public float health = 100f;
    //public static int count = 0;
    public Animator anim;
    public float fwdSpeed;
    public GameObject player;
    public AudioSource audio;
    public AudioClip ughAudio;
    public AudioClip painAudio;
    public RoomCheck roomCheck;
    public float timeTest;
    public float cooldown=0;

    public int layerMask = 1<<11;

    //public float time;
    private Vector3 offset;
    private bool isChasing = false;
    private bool dead = false;
    private bool attacking = false;
    private bool takingDmg = false;

    private void Awake()
    {
        //count++;
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player");
       // layerMask = ~layerMask;

        offset.y = 1;
    }

    private void Update()
    {
        var lookPos = player.transform.position - transform.position;
        lookPos.y = 0;
        var rotation = Quaternion.LookRotation(lookPos);
        if (!dead)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 100);
        }
        if (isChasing && !dead && !takingDmg && !attacking)
        {
            transform.Translate(0, 0, fwdSpeed * Time.deltaTime);
            anim.Play("walk");
        }

        RaycastHit hit;
        if (Physics.Raycast(gameObject.transform.position+offset, gameObject.transform.forward, out hit, range, layerMask))
        {
            //Debug.Log(hit.transform.tag);
            Debug.DrawRay(transform.position+offset, transform.forward*hit.distance, Color.green);
            if (hit.transform.tag == "Player" && cooldown <= 0 && !dead)
            {
                cooldown = 3;
                StartCoroutine(Animate(1.3f, "jumpBiteAttack_RM"));
            }
        }
        cooldown -= Time.deltaTime;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!dead && other.tag == "Atract")
        {
            isChasing = true;
        }
    }

    public void TakeDamage(float dmg)
    {
        if (!dead)
        {
            isChasing = true;
            health -= dmg;

            if (health <= 0f)
            {
                roomCheck.EnemyDown();
                Die();
            }
            else
            {
                StartCoroutine(Animate(1.0f, "getHit2"));                
            }
        }
    }
    
    IEnumerator Animate(float _time, string animName)
    {
        if (animName == "death")
        {
            dead = true;
            anim.Play(animName);
            audio.PlayOneShot(painAudio);
            yield return new WaitForSeconds(_time);
            Destroy(gameObject);
        }
        if(animName == "getHit2")
        {
            takingDmg = true;
            anim.Play("getHit2");
            audio.PlayOneShot(ughAudio);
            yield return new WaitForSeconds(_time);
            takingDmg = false;
        }
        if(animName == "walk")
        {
            anim.Play(animName);
            yield return new WaitForSeconds(_time);
        }
        if (animName == "jumpBiteAttack_RM")
        {
            attacking = true;
            anim.Play(animName);
            yield return new WaitForSeconds(_time);
            attacking = false;
        }
    }

    void Die()
    {
        StartCoroutine(Animate(5, "death"));
    }

    public bool GetDamage()
    {
        if (!dead)
            return false;
        else
            return true;
    }
}
