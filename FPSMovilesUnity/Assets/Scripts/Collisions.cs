﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour {

    public GameObject player;

    [SerializeField]
    private PlayerMec playermec;

    private void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.tag == "Enemy")
        {
        bool enemyDead = collisionInfo.collider.transform.root.GetComponent<Enemy>().GetDamage();

            if(!enemyDead)
                playermec.TakeDamage();
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Portal")
        {
            player.transform.position = other.GetComponent<Portals>().Teleport().position;            
        }
    }
}
