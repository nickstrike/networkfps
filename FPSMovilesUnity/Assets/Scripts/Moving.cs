﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Moving : NetworkBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float RunSpeed;
    private bool running = false;
    private float normalspeed ;
    public Joystick joystick;
    [SerializeField]
    private AudioClip mover;
    bool isWalking;
    //testing
    private Vector3 moveDirection = Vector3.zero;
    void Start()
    {
        normalspeed = speed;

    }
    void Update()
    {        
        if(!isLocalPlayer)
        { return; }

        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
#if !UNITY_ANDROID
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
#else
            joystick.gameObject.SetActive(true);
            moveDirection = new Vector3(joystick.Horizontal, 0, joystick.Vertical);
#endif
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            isWalking = true;
            if (speed == 0)
                isWalking = false;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;
            if (Input.GetKey(KeyCode.LeftShift)){
                running = true;

            }
            else{
                running = false;
            }
            if (running)
            {
                speed = RunSpeed;
                GetComponent<AudioSource>().PlayOneShot(mover);
            }
            else
            {
                speed = normalspeed;
            }
        }
        if(isWalking == true)
        {
            
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);
    }
}