﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomCheck : MonoBehaviour {

    [SerializeField]
    private GameObject portal;
    [SerializeField]
    private int enemiesLeft;

    public void EnemyDown()
    {
        enemiesLeft--;

        if(enemiesLeft <= 0)
        {
            portal.SetActive(true);
        }
    }
}
