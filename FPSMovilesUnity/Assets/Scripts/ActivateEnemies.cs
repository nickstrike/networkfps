﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateEnemies : MonoBehaviour {

    public Enemy[] enemies;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            for(int i=0;i<enemies.Length;i++)
            {
                enemies[i].TakeDamage(0);
            }

            gameObject.SetActive(false);
        }
    }
}
