﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMec : NetworkBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletTransform;
    public Image healthBar;

    public int hp = 100;
    public const int maxHealth = 100;

    public Transform respawnPoint;

    float cooldownDamage = 0;
    float respawnCooldown = 0;
    [SerializeField]
    private AudioClip hurtSound;
    [SerializeField]
    private AudioSource sound;

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        cooldownDamage += Time.deltaTime;
    }

    public void TakeDamage()
    {
        if (!isServer)
            return;

        if (cooldownDamage >= 0)
        {
            hp -= 35;
            cooldownDamage = -3;
            sound.PlayOneShot(hurtSound);
        }

        if (hp <= 0)
        {
            StartCoroutine(Die());
        }
    }

    IEnumerator Die()
    {
        respawnCooldown += 3;
        yield return new WaitForSeconds(respawnCooldown);
    }

    void OnChangeHealth(int health)
    {
        healthBar.fillAmount = ((float)health / (float)maxHealth);
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (gameObject.tag == "Enemy")
        Destroy(gameObject);
    }
}