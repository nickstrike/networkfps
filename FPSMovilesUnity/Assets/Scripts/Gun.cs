﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using EZCameraShake;

public class Gun : MonoBehaviour {

    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
	public int maxAmmo = 10;
	public float reloadTime = 1f;
    public AudioClip shotAudio;
    public AudioClip reloadAudio;
    public Touch touch;
	public Camera fpsCam;
    public ParticleSystem muzzle;
    public GameObject impactEffect;
    public GameObject impactEffectBlood;
    public int layerMask = 1 << 11;
	public Animator animator;
    public GameObject ammoHud;


    private bool isReloading = false;
	public int currentAmmo;
	private float nextTimeToFire = 0f;
    private AudioSource audioSource;
   
    private void Awake() 
    {
        currentAmmo = maxAmmo;
        layerMask = ~layerMask;
        audioSource = GetComponent<AudioSource>();   
    }

	private void OnEnable() {
        ammoHud.SetActive(true);
        isReloading = false;
		animator.SetBool("Reloading", false);
	}

    private void OnDisable()
    {
        if(ammoHud != null)
        ammoHud.SetActive(false);
    }


    // Update is called once per frame
    void Update () 
	{
		if(isReloading)
			return;

        if (currentAmmo <= 0 || (Input.GetKeyDown(KeyCode.R) && currentAmmo < maxAmmo))
        {
            StartCoroutine(Reload());
            return;
        }

#if UNITY_ANDROID 

        //movil
        if(Input.touchCount > 0 && Time.time >= nextTimeToFire)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                touch = Input.GetTouch(i);
                if (touch.position.y >= Screen.height / 2)
                {
                    nextTimeToFire = Time.time + 1f / fireRate;
                    Shoot();
                }
            }
        }

#else
        if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f / fireRate;
            Shoot();
            
        }
#endif
    }

    void Shoot()
    {		
		audioSource.PlayOneShot(shotAudio, 0.3f);
		currentAmmo--;
        muzzle.Play();
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range, layerMask))
        {
            //Debug.Log(hit.transform.name);
            Enemy target = hit.collider.transform.root.GetComponent<Enemy>();
            if(target != null)
            {
                target.TakeDamage(damage);

            }

            if(hit.collider.tag != "Enemy")
            { 
                GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGO, 2f);
            }
            else
            {
                GameObject impactGO = Instantiate(impactEffectBlood, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impactGO, 2f);
            }
        }
		
    }

	IEnumerator Reload()
	{
		isReloading = true;
		//Debug.Log("Reloading...");

		animator.SetBool("Reloading", true);
        audioSource.PlayOneShot(reloadAudio);

        yield return new WaitForSeconds(reloadTime - .25f);
        

        animator.SetBool("Reloading", false);
		yield return new WaitForSeconds(.25f);

        currentAmmo = maxAmmo;
		isReloading = false;

	}
}